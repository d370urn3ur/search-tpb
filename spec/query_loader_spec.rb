require_relative 'spec_helper.rb'

describe QueryLoader do

  let(:basic) { "spec/query_data/test_database_basic.rb" }
  let(:empty_lines) { "spec/query_data/test_database_empty_lines.rb" }
  let(:comments) { "spec/query_data/test_database_comments.rb" }
  let(:bad_data) { "spec/query_data/test_database_bad_data.rb" }

  describe '#load' do

    it "tests basic query format" do
      queries = subject.load(basic)
      expect(queries.size).to eq(2)
    end

    it "tests empty lines" do
      queries = subject.load(empty_lines)
      expect(queries.size).to eq(2)
    end

    it "tests comments" do
      queries = subject.load(comments)
      expect(queries.size).to eq(2)
    end

    it "tests bad data" do
      queries = subject.load(bad_data)
      expect(queries.size).to eq(3)
      queries.each do |query|
        expect(query.title).to be
        expect(query.searchTerm).to be
        expect(query.type).to be
        expect(query.season).to be
        expect(query.episode).to be
      end
    end

  end

end
