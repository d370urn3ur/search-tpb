class Job
  
  def initialize(dataSource, crawler, scraper, query)
    @dataSource = dataSource
    @crawler = crawler
    @scraper = scraper
    @query = query
  end

  def run

    resultHash = Hash.new

    begin

      searchPage = @crawler.crawlSearch(@query)
      searchResults = @scraper.scrapeSearch(searchPage)
      return if searchResults.nil?
      searchResults.each do |searchResult|
        detailPage = @crawler.crawlDetail(searchResult, @dataSource.baseUri)
        magnet, scrapedResult = @scraper.scrapeDetail(detailPage, searchResult)
        return if magnet.nil?
        resultHash[magnet] = scrapedResult
      end

      unless resultHash.empty?
        return { @query.title => resultHash }
      end

    rescue Exception => e
      puts "#{@dataSource.name} failed"
      puts e.message
      puts e.backtrace
    end

  end

end
