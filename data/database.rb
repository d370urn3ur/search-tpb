Full Frontal, full frontal s07e04, series, 7, 4
Last Week Tonight, last week tonight s09e01, series, 9, 1
The Orville, the orville s03e01, series, 3, 1
Dalziel and Pascoe, dalziel and pascoe s10e01, series, 1, 0
Star Trek: Strange New Worlds, star trek strange new worlds s01e01, series, 1, 1
The Expanse, the expanse s06e07, series, 6, 7
Star Trek - Discovery, star trek discovery s04e08, series, 4, 8
# part 6 is newest
# JoJos Bizarre Adventure, jojos bizarre adventure, series, 0, 0
The Righteous Gemstones, the righteous gemstones s02e06, series, 2, 6
Claws, claws s04e10, series, 3, 10
Better Call Saul, better call saul s06e01, series, 6, 1
Star Trek Picard, picard s02e01, series, 2, 1
# Ozark, ozark s04, series, 4, 0
Star Trek: Lower Decks, star trek lower decks s03e01, series, 3, 1

# I Think you should leave, i think you should leave s03, series, 3, 0
# Search Party, search party s06, series, 6, 0
# Its always sunny in Philadelphia, always sunny s16e01, series, 16, 1
# Succession, succession s04e01, series, 4, 1
# Shetland, shetland s07e01, series, 7, 1
# The Witcher, the witcher s03, series, 3, 0
# The Great, the great s03, series, 3, 0
# Curb Your Enthusiasm, curb your enthusiasm s12e01, series, 12, 1
# Rick and Morty, rick and morty s06e01, series, 6, 1
# Baptiste, baptiste s03e01, series, 3, 1
# AP Bio, a.p. bio s05e01, series, 5, 1
# Devs, devs s02e01, series, 2, 1
# Mythic Quest, mythic quest s03e01, series, 3, 01
# Girls5eva, girls5eva s02, series, 2, 0
# Snowfall, snowfall s05e01, series, 5, 1
# Un village français, un village francais s07, series, 7, 0
# Bridgerton, bridgerton s02e01, series, 2, 1
# For All Mankind, for all mankind s03e01, series, 3, 1
# Back, back s03e01, series, 3, 1
# Engrenages, engrenages s09e01, series, 9, 1
# Fargo, fargo s05e01, series, 5, 1
# Lovecraft Country, lovecraft country s02e01, series, 2, 1
# The Mandalorian, mandalorian s03e01, series, 3, 1
# The Boys, the boys s03e01, series, 3, 1
# Babylon Berlin, babylon berlin s04, series, 4, 0
# Truth Seekers, truth seekers s02e01, series, 2, 1
# Outlander, ​outlander s06e01, series, 6, 1
# Westworld, westworld s04e01, series, 4, 1
# Hanna, hanna s03e01 hevc, series, 3, 1
# The Last Kingdom, the last kingdom s05e01, series, 5, 1
# Belgravia, belgravia s02e01, series, 2, 1
# The Marvelous Mrs Maisel, the marvelous mrs maisel s04e01, series, 4, 1
# Preacher, preacher s05, series, 5, 0
# Lodge 49, lodge 49 s03e01, series, 3, 1
# Undone, undone s02, series, 2, 0
# Stranger Things, stranger things s04, series, 4, 0
# Barry, barry s03e01, series, 3, 1
# Catastrophe, catastrophe s05e01, series, 4, 1
# Russian Doll, russian doll s02, series, 2, 0
# Camping, camping s02e01, series, 2, 1
# Comrade Detective, comrade detective s02e01, series, 2, 1
# The Night Of, the night of s02e01, series, 2, 1
# Inside Amy Schumer, inside amy schumer s05e01, series, 5, 1
# The Comeback, the comeback s03e01, series, 3, 1
# Atlanta, atlanta s03e01, series, 3, 1
# Dublin Murders, dublin murders s02e01, series, 2, 1