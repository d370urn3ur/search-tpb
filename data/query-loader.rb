Query = Struct.new(:title, :searchTerm, :type, :season, :episode)

class QueryLoader
  def load(filename)
    queries = []
    IO.foreach(filename) do |line|
      item = line.strip
      next if item.start_with? '#'
      next if item.empty?
      components = item.split(',')
      queries << Query.new(
        (components[0] || '').strip,
        (components[1] || '').strip,
        (components[2] || '').strip,
        (components[3] || '').strip,
        (components[4] || '').strip
      )
    end
    queries
  end
end
