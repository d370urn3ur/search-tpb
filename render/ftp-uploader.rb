require 'net/ftp'

SERVER = 'ftpperso.free.fr'
USERNAME = ''
PASSWORD = ''
UPLOAD_PATH = '/test'

class FtpUploader

  def uploadFiles(files)
    begin
      ftp = connect()
      verifyPathExists(ftp)
      files.each do |fileName|
        uploadFile(ftp, fileName)
      end
    rescue Exception => e
      puts e.message
    end
  end

  private

  def connect()
    Net::FTP.open(SERVER, USERNAME, PASSWORD)
  end

  def verifyPathExists(ftp)
    files = ftp.list(UPLOAD_PATH)
    puts "#{UPLOAD_PATH}:"
  end

  def uploadFile(ftp, fileName)
    puts "uploading file -> #{fileName} to #{SERVER}"
    begin
      file = File.new(fileName)
      ftp.putbinaryfile(file, "#{UPLOAD_PATH}/#{File.basename(file)}")
      puts "uploaded: #{fileName}"
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
    end
  end

end
