class HtmlWriter

  def writeResults(searchResults)
    html = ''
    htmlBegin(html)
    searchResults.each do |feedTitle, resultHash|
      addTable(html, feedTitle, resultHash)
    end
    htmlEnd(html)
    fileName = "output/index.html"
    File.write(fileName, html)
    [fileName]
  end

  private

  def htmlBegin(html)
    html.concat('<!doctype html>')
    html.concat('<html>')
    html.concat('<head>')
    html.concat('<meta charset="UTF-8" />')
    html.concat('<meta name="viewport" content="width=device-width, initial-scale=1.0" />')
    html.concat('<link rel="stylesheet" type="text/css" href="index.css" />')
    html.concat('<title>Your weekly search results</title>')
    html.concat('</head>')
    html.concat('<body>')
  end

  def addTable(html, headerTitle, resultHash)
    html.concat("<header>#{headerTitle}</header>")
    html.concat('<table>')
    html.concat('<thead><tr><th class="colName">Name</th><th class="colSeeders">Seeders</th><th class="colLeechers">Leechers</th></tr></thead>')
    html.concat('<tbody>')
    resultHash.each_with_index do |(magnet, searchResult),index|
      addRow(html, index, searchResult, magnet)
    end
    html.concat('</tbody>')
    html.concat('</table>')
    html.concat('<br/><br/>')
  end

  def addRow(html, index, searchResult, magnet)
      parity = index.odd? ? 'odd' : 'even'
      html.concat("<tr class='#{parity}'>")
      html.concat("<td><a href='#{magnet}'>#{searchResult.name}</a></td>")
      html.concat("<td>#{searchResult.seeders}</td>")
      html.concat("<td>#{searchResult.leechers}</td>")
      html.concat('</tr>')
  end

  def htmlEnd(html)
    html.concat('</body>')
    html.concat('</html>')
  end

end
