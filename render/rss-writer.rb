class RssWriter

  def writeResults(searchResults)
    fileNames = []
    searchResults.each do |feedTitle, resultHash|
      rss = ''
      rssBegin(rss, feedTitle)
      resultHash.each do |magnet, searchResult|
        addItem(rss, searchResult, magnet)
      end
      rssEnd(rss)
      fileName = "#{feedTitle}.xml"
      File.write(fileName, rss)
      fileNames << fileName
    end
    fileNames
  end

  private

  def rssBegin(rss, feedTitle)
    rss.concat('<rss version="2.0">')
    rss.concat("<channel><title>#{feedTitle.encode(:xml => :text)}</title><link></link><description></description>")
  end

  def addItem(rss, searchResult, magnet)
    rss.concat("<item>")
    rss.concat("<title>#{searchResult.name.encode(:xml => :text)} | #{searchResult.seeders.encode(:xml => :text)} | #{searchResult.leechers.encode(:xml => :text)}</title>")
    rss.concat("<link>#{magnet.encode(:xml => :text)}</link>")
    rss.concat("</item>")
  end

  def rssEnd(rss)
    rss.concat("</channel>")
    rss.concat('</rss>')
  end

end
