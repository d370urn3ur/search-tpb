require 'open-uri'
require 'addressable'
require 'nokogiri'
require_relative 'scraper-eztv'
require_relative '../job'
require_relative '../datasource'
require_relative '../crawler/crawler'

class EztvCrawler < Crawler

  def initialize(queries, jobQueue)
    super(
      queries,
      jobQueue,
      DataSource.new('EZTV', URI.parse('https://eztv.io')),
      EztvScraper.new
    )
  end

  protected

  def buildSearchUri(dataSource, query)
    baseUri = dataSource.baseUri.clone
    encodedSearchTerm = Addressable::URI.escape(query.searchTerm)
    baseUri.path = "/search/#{encodedSearchTerm}"
    baseUri
  end

  def fetchSearchPage(searchUri)
    begin
      Nokogiri::HTML(searchUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

  def buildDetailUri(baseUri, relativeUri)
    baseUri.query = nil
    # its relative to base
    baseUri.path = relativeUri
    baseUri
  end

  def fetchDetailPage(detailUri)
    begin
      Nokogiri::HTML(detailUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

end
