require_relative '../searchresult'

class EztvScraper

  def scrapeSearch(searchPageHtml)
    rows = findSearchResultRows(searchPageHtml)
    puts "got #{rows.size} rows"
    return if rows.nil? || rows.empty?
    extractSearchResults(rows)
  end

  def scrapeDetail(detailPageHtml, searchResult)
    if(detailName = extractNameFromDetail(detailPageHtml))
      searchResult.name = detailName
    end
    if(magnet = extractMagnetFromDetail(detailPageHtml))
      puts "got magnet: #{magnet} for #{searchResult.name}"
      return magnet, searchResult
    end
  end

  private

  def findSearchResultRows(html)
    html.css("table > tr.forum_header_border")
  end

  def extractSearchResults(rows)
    parsed = []
    rows.each do |row|
      anchor = row.css('a.epinfo')[0]
      next if anchor.nil?
      name = anchor['title']
      relativeUri = anchor['href']
      next if name.nil? || relativeUri.nil?
      seeders = row.css('td.forum_thread_post_end').inner_html unless row.css('td.forum_thread_post_end').nil?
      leechers = '-'
      parsed << SearchResult.new(name, relativeUri, seeders, leechers)
    end
    parsed
  end

  def extractNameFromDetail(html)
    html.css('td.section_post_header > h1 > span').inner_html
  end

  def extractMagnetFromDetail(html)
    links = html.css('a')
    links.each do |link|
      href = link['href']
      if href.start_with? 'magnet'
        return href
      end
    end
    nil
  end

end
