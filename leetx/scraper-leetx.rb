require_relative '../searchresult'

class LeetxScraper

  def scrapeSearch(searchPageHtml)
    rows = findSearchResultRows(searchPageHtml)
    puts "got #{rows.size} rows"
    return if rows.nil? || rows.empty?
    extractSearchResults(rows)
  end

  def scrapeDetail(detailPageHtml, searchResult)
    if(detailName = extractNameFromDetail(detailPageHtml))
      searchResult.name = detailName
    end
    if(magnet = extractMagnetFromDetail(detailPageHtml))
      puts "got magnet: #{magnet} for #{searchResult.name}"
      return magnet, searchResult
    end
  end

  private

  def findSearchResultRows(html)
    html.css("table > tbody > tr")
  end

  def extractSearchResults(rows)
    parsed = []
    rows.each do |row|
      anchor = row.css('td.coll-1.name > a')[1]
      next if anchor.nil?
      name = anchor.inner_html
      relativeUri = anchor['href']
      next if name.nil? || relativeUri.nil?
      seeders = row.css('td.coll-2.seeds').inner_html unless row.css('td.coll-2.seeds').nil?
      leechers = row.css('td.coll-3.leeches').inner_html unless row.css('td.coll-3.leeches').nil?
      parsed << SearchResult.new(name, relativeUri, seeders, leechers)
    end
    parsed
  end

  def extractNameFromDetail(html)
    html.css('div.col-9.page-content > div.box-info.torrent-detail-page.vpn-info-wrap > div.box-info-heading.clearfix > h1').inner_html
  end

  def extractMagnetFromDetail(html)
    links = html.css('div.col-9.page-content a')
    links.each do |link|
      href = link['href']
      if href.start_with? 'magnet'
        return href
      end
    end
    nil
  end

end
