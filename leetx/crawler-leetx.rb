require 'open-uri'
require 'addressable'
require 'nokogiri'
require_relative 'scraper-leetx'
require_relative '../job'
require_relative '../datasource'
require_relative '../crawler/crawler'

class LeetxCrawler < Crawler

  def initialize(queries, jobQueue)
    super(
      queries,
      jobQueue,
      DataSource.new('1337x', URI.parse('https://1337x.to')),
      LeetxScraper.new
    )
  end

  protected

  def buildSearchUri(dataSource, query)
    baseUri = dataSource.baseUri.clone
    encodedSearchTerm = Addressable::URI.escape(query.searchTerm)
    baseUri.path = "/search/#{encodedSearchTerm}/1/"
    baseUri
  end

  def fetchSearchPage(searchUri)
    begin
      Nokogiri::HTML(searchUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

  def buildDetailUri(baseUri, relativeUri)
    baseUri.query = nil
    # its relative to base
    baseUri.path = relativeUri
    baseUri
  end

  def fetchDetailPage(detailUri)
    begin
      Nokogiri::HTML(detailUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

end
