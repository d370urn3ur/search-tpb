class JobQueue

  @@NumThreads = 8

  def initialize(searchResults)

    @searchResults = searchResults

    mutex = Mutex.new
    @queue = Queue.new
    @workers = Array.new(@@NumThreads) do |n|
      Thread.new do
        while job = @queue.deq
          result = job.run
          unless result.nil?
            mutex.synchronize do
              @searchResults.merge!(result) do |key, old, new|
                old.merge!(new)
              end
            end
          end
        end
      end
    end
  end

  def enqueue(job)
    @queue << job
  end

  def shutdown
    @queue.close
    @workers.each(&:join)
  end

end
