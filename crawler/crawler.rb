class Crawler

  @@UserAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:69.0) Gecko/20100101 Firefox/69.0'

  def initialize(queries, jobQueue, dataSource, scraper)
    @queries = queries
    @jobQueue = jobQueue
    @dataSource = dataSource
    @scraper = scraper
  end

  def crawl
    @queries.each do |query|
      job = Job.new(@dataSource, self, @scraper, query)
      @jobQueue.enqueue(job)
    end
  end

  def crawlSearch(query)
    searchUri = buildSearchUri(@dataSource, query)
    puts "searching: #{searchUri}"
    fetchSearchPage(searchUri)
  end

  def crawlDetail(searchResult, baseUri)
    detailUri = buildDetailUri(baseUri, searchResult.relativeUri)
    fetchDetailPage(detailUri)
  end

  protected

  def buildSearchUri(dataSource, query)
    # TODO: use rspec for these
  end

  def fetchSearchPage(searchUri)
    # TODO: use rspec for these
  end

  def buildDetailUri(baseUri, relativeUri)
    # TODO: use rspec for these
  end

  def fetchDetailPage(detailUri)
    # TODO: use rspec for these
  end

end
