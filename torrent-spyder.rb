#!/usr/bin/env ruby

require_relative 'data/query-loader'
require_relative 'leetx/crawler-leetx'
require_relative 'eztv/crawler-eztv'
require_relative 'lime/crawler-lime'
require_relative 'render/html-writer'
require_relative 'jobqueue'
require 'benchmark'

time = Benchmark.realtime {

  queryLoader = QueryLoader.new
  queries = queryLoader.load('data/database.rb')
  puts "database is: #{queries}"

  searchResults = Hash.new
  jobQueue = JobQueue.new(searchResults)

  crawlers = [
    LeetxCrawler.new(queries, jobQueue),
    # EztvCrawler.new(queries, jobQueue),
    LimeCrawler.new(queries, jobQueue)
  ]

  crawlers.each do |crawler|
    crawler.crawl
  end

  jobQueue.shutdown

  writer = HtmlWriter.new
  outputFiles = writer.writeResults(searchResults)
}
puts "finished: took #{time} seconds"
