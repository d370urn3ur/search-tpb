require_relative '../searchresult'

class LimeScraper

  def scrapeSearch(searchPageHtml)
    rows = findSearchResultRows(searchPageHtml)
    puts "got #{rows.size} rows"
    return if rows.nil? || rows.empty?
    extractSearchResults(rows)
  end

  def scrapeDetail(detailPageHtml, searchResult)
    if(detailName = extractNameFromDetail(detailPageHtml))
      searchResult.name = detailName
    end
    if(magnet = extractMagnetFromDetail(detailPageHtml))
      puts "got magnet: #{magnet} for #{searchResult.name}"
      return magnet, searchResult
    end
  end

  private

  def findSearchResultRows(html)
    html.css("table.table2 > tr")
  end

  def extractSearchResults(rows)
    parsed = []
    rows.each do |row|
      anchor = row.css('td.tdleft > div.tt-name > a')[1]
      next if anchor.nil?
      name = anchor.inner_html
      relativeUri = anchor['href']
      next if name.nil? || relativeUri.nil?
      seeders = row.css('td.tdseed').inner_html unless row.css('td.tdseed').nil?
      leechers = row.css('td.tdleech').inner_html unless row.css('td.tdleech').nil?
      parsed << SearchResult.new(name, relativeUri, seeders, leechers)
    end
    parsed
  end

  def extractNameFromDetail(html)
    html.css('div#content > h1')[0].inner_html
  end

  def extractMagnetFromDetail(html)
    links = html.css('div.dltorrent > p > a')
    links.each do |link|
      href = link['href']
      if href.start_with? 'magnet'
        return href
      end
    end
    nil
  end

end
