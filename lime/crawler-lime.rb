require 'open-uri'
require 'cgi'
require 'addressable'
require 'nokogiri'
require_relative 'scraper-lime'
require_relative '../job'
require_relative '../datasource'
require_relative '../crawler/crawler'

class LimeCrawler < Crawler

  def initialize(queries, jobQueue)
    super(
      queries,
      jobQueue,
      DataSource.new('Lime', URI.parse('https://www.limetorrents.info')),
      LimeScraper.new
    )
  end

  def crawl
    @queries.each do |query|
      job = Job.new(@dataSource, self, @scraper, query)
      sleep(5)
      @jobQueue.enqueue(job)
    end
  end

  protected

  def buildSearchUri(dataSource, query)
    baseUri = dataSource.baseUri.clone
    encodedSearchTerm = Addressable::URI.escape(query.searchTerm)
    baseUri.path = "/search/all/#{encodedSearchTerm}/"
    baseUri
  end

  def fetchSearchPage(searchUri)
    begin
      Nokogiri::HTML(searchUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

  def buildDetailUri(baseUri, relativeUri)
    detailUri = baseUri.clone
    detailUri.query = nil
    if relativeUri.start_with? '/'
      relativeUri = relativeUri[1..-1]
    end
    relativeUri = CGI.escape(relativeUri)
    URI.join(detailUri, relativeUri)
  end

  def fetchDetailPage(detailUri)
    begin
      Nokogiri::HTML(detailUri.open('User-Agent' => @@UserAgent))
    rescue OpenURI::HTTPRedirect => redirect
      puts "redirect error, redirecting manually to: #{redirect.uri.to_s}"
      Nokogiri::HTML(redirect.uri.open('User-Agent' => @@UserAgent))
    end
  end

end
